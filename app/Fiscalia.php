<?php

namespace Vehiculos;

use Illuminate\Database\Eloquent\Model;

class Fiscalia extends Model
{
    protected $fillable = ['descripcion','activo'];
}
