<?php

namespace Vehiculos\Http\Controllers;

use Auth;
use Vehiculos\Fiscalia;
use Illuminate\Http\Request;

class FiscaliaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $fiscalias = Fiscalia::
      where('activo',1)
      ->orderBy('descripcion','ASC')->paginate(10);

      return view('Catalogos.fiscalias_index', compact('fiscalias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      return view('Catalogos.fiscalias_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,['descripcion'=> 'required','activo'=> 'required']);
      Fiscalia::create($request->all());

      return redirect()->route('fiscalias.index')->with('success', 'Registro creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fiscalia = Fiscalia::find($id);
        return view('Catalogos.fiscalias_edit',compact('fiscalia'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['descripcion'=> 'required','activo'=> 'required']);

        fiscalia::find($id)->update($request->all());
        return redirect()->route('fiscalias.index')->with('success','Registro Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Fiscalia::find($id)->delete();
        return redirect()->route('fiscalias.index')->with('success','Registro Eliminado Exitosamente');
    }
}
