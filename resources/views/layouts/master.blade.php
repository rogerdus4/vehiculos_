<!DOCTYPE html>
<html lang="{{app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Vehiculos') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
  <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Vehiculos') }}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Authentication Links -->
        @guest
            <a class="nav-link" href="{{ route('login') }}">Login</a>
        @else
        <ul class="nav navbar-nav navbar-right">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Catalogos
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('fiscalias.index') }}">Fiscalias</a>
              <a class="dropdown-item" href="">Agencias</a>
              <a class="dropdown-item" href="#">Usuarios</a>
              <a class="dropdown-item" href="#"></a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Vehiculos
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Validar</a>
              <a class="dropdown-item" href="#">Folios C5</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Asignar Masivo</a>
              <a class="dropdown-item" href="#">Asignar Individual</a>
              <a class="dropdown-item" href="#">Desasignar</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Reportes
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Consulta NUC</a>
              <a class="dropdown-item" href="#">Consulta Vehiculos</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Reporte PDF</a>
              <a class="dropdown-item" href="#">Reporte Excel</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Movimientos</a>
            </div>
          </li>
          <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
        </ul>-->
          <!--<a class="nav-link" href="{{ route('register') }}">Register</a>-->
            <li class="dropdown" aria-labelledby="navbarDropdown">
                <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="{{ route('logout') }}" class="dropdown-item"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                </div>
            </li>
              </ul>
        @endguest
    </div>
  </nav>
  @yield('content')
  <!-- Scripts -->
</div>
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
