@extends('layouts.master')
  @section('content')
  <main class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content" role="main">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Error!</strong> Revise los campos obligatorios.<br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
            </div>
            @endif
            @if(Session::has('success'))
              <div class="alert alert-info">
                {{Session::get('success')}}
              </div>
            @endif

            <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Nueva Fiscalía</h3>
                    </div>
              <div class="panel-body">
                <div class="table-container">
                  <form method="POST" action="{{ route('fiscalias.store') }}" role="form" >
                    {{ csrf_field() }}
                        <div class="form-group">
                           <label for="descripcion">Nombre de Fiscalia</label>
                          <input type="text" name="descripcion" class="form-control" placeholder="Nombre de Fiscalia">
                        </div>
                        <div class="form-group">
                         <label for="exampleFormControlSelect1">Activo</label>
                         <select name="activo" class="form-control">
                           <option value="1">Activo</option>
                           <option value="0">Inactivo</option>
                         </select>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="submit" value="Guardar" class="btn btn-success btn-block">
                            <a href="{{ route('fiscalias.index') }}" class="btn btn-info btn-block" >Atrás</a>
                          </div>
                        </div>
                  </form>
                </div>
            </div>
          </div>
      </div>
    </div>
  </section>
  @endsection
