@extends('layouts.master')

@section('content')
<main class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content" role="main">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Error!</strong> Revise los campos obligatorios.<br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
            </div>
            @endif
            @if(Session::has('success'))
              <div class="alert alert-info">
                {{Session::get('success')}}
              </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Fiscalias</h3></div>
                <div class="panel-body">
                  <div class="pull-left"> </div>
                  <div class="pull-right">
                    <div class="btn-group">
                      <a href="{{ route('fiscalias.create') }}" class="btn btn-info">Añadir Fiscalia </a>
                    </div>
                  </div>
                <div class="container">
                  <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                      <th>Id</th>
                      <th>Agencia</th>
                      <th>Tipo</th>
                      <th>Editar</th>
                      <th>Eliminar</th>
                    </thead>
                    <tbody>
                      @if($fiscalias->count())
                      @foreach($fiscalias as $fiscalia)
                        <tr>
                          <td>{{$fiscalia->id}}</td>
                          <td>{{$fiscalia->descripcion}}</td>
                          <td>{{$fiscalia->activo}}</td>
                          <td><a class="btn btn-warning" href="{{action('FiscaliaController@edit', $fiscalia->id)}}">*</a></td>
                          <td>
                              <form action="{{ action('FiscaliaController@destroy', $fiscalia->id )}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">

                                <button class="btn btn-danger btn-xs" type="submit"> - </button>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                      @else
                      <tr>
                        <td colspan="8">No hay Registros</td>
                      </tr>
                      @endif
                  </tbody>
                  </table>
                </div>
              </div>
            </div>
            {{ $fiscalias->links() }}
        </div>
    </div>
</main>
@endsection
